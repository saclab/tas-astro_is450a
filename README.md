
# IS450a

## Ressource list:

### Subjects and slides: 

* [BE : Space Systems Introduction](https://gitlab.isae-supaero.fr/saclab/tas-astro_is450a/-/raw/main/docs/Space_Systems_Architecture_BE.pdf?ref_type=heads&inline=false)

* [Support slides: Space Systems Introduction (EN)](https://gitlab.isae-supaero.fr/saclab/tas-astro_is450a/-/raw/main/docs/Intro_BE_Minisat_2024.pdf?ref_type=heads&inline=false)

* [BE: End Of Life](https://gitlab.isae-supaero.fr/saclab/tas-astro_is450a/-/raw/main/docs/BE_ISAE_EndOfLife_EN.pdf?ref_type=heads&inline=false)

* [Support slides: Power](https://gitlab.isae-supaero.fr/saclab/tas-astro_is450a/-/raw/main/docs/support/PowerJeremieChaix.pdf?ref_type=heads&inline=false)

### Software available in the rooms, but also installer for personal laptops:  

* [Software - Satorb -  v7.0](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/softs/Satorb/Satorb_V07_setup.exe?inline=false)

* [Software - Simusat - V6.4](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/raw/master/setup/SetupSimusat/Release/SetupSimusat.msi?inline=false)

* [VTS - 3.7.0 "Night"](https://timeloop.fr/vts/download/)


### Link to the LMS

* [LMS page for the TAS Astro - Space Systems Engineering 2023-2024](https://lms.isae.fr/course/view.php?id=3989#section-4)

* [Uploading your report on LMS](https://lms.isae.fr/mod/assign/view.php?id=107572)



### Other version, other softwares

* [Software - Stela - v3.5.1](https://gitlab.isae-supaero.fr/saclab/tas-astro_is450a/-/raw/main/softs/stela-install-3.5.1.jar?ref_type=heads&inline=false)

* [Python - Anaconda distribution](https://www.anaconda.com/)

* [Software - Simusat (old version,  power simulation) - VB6 - V03](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/softs/oldVersions/Simusat_Vb6_V03_setup.exe?inline=false)

* [Software - Simusat - Stand-alone v6.4.3](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/packages/25)


<!-- !https://sourceforge.isae.fr/attachments/download/2699/satorb04.jpg! -->

[<img src="./docs/img/satorb04.jpg"/>](./docs/img/satorb04.jpg)


# Repository Tree Organisation:

```
.
├── docs
├── REAMDE.md
├── softs
└── supportLight
```

# Get all GIT repository

```
git clone https://gitlab.isae-supaero.fr/saclab/tas-astro_is450a.git

```



